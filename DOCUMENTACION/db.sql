-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema proyectonevada
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema proyectonevada
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `proyectonevada` DEFAULT CHARACTER SET utf8 ;
USE `proyectonevada` ;

-- -----------------------------------------------------
-- Table `proyectonevada`.`camaras`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyectonevada`.`camaras` (
  `idCAMARAS` INT(11) NOT NULL AUTO_INCREMENT,
  `altitud` FLOAT NULL DEFAULT NULL,
  `longitud` FLOAT NULL DEFAULT NULL,
  `latitud` FLOAT NULL DEFAULT NULL,
  `descripcion` VARCHAR(45) NULL DEFAULT NULL,
  `nombre` VARCHAR(45) NULL,
  PRIMARY KEY (`idCAMARAS`))
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `proyectonevada`.`usuarios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyectonevada`.`usuarios` (
  `idUSUARIOS` INT(11) NOT NULL AUTO_INCREMENT,
  `usuario` VARCHAR(45) NULL DEFAULT NULL,
  `contraseña` VARCHAR(45) NULL DEFAULT NULL,
  `nombre` VARCHAR(45) NULL DEFAULT NULL,
  `apellidos` VARCHAR(45) NULL DEFAULT NULL,
  `correo` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`idUSUARIOS`))
ENGINE = InnoDB
AUTO_INCREMENT = 12
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `proyectonevada`.`camaras_has_usuarios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyectonevada`.`camaras_has_usuarios` (
  `CAMARAS_idCAMARAS` INT(11) NOT NULL,
  `USUARIOS_idUSUARIOS` INT(11) NOT NULL,
  PRIMARY KEY (`CAMARAS_idCAMARAS`, `USUARIOS_idUSUARIOS`),
  INDEX `fk_CAMARAS_has_USUARIOS_USUARIOS1_idx` (`USUARIOS_idUSUARIOS` ASC),
  INDEX `fk_CAMARAS_has_USUARIOS_CAMARAS1_idx` (`CAMARAS_idCAMARAS` ASC),
  CONSTRAINT `fk_CAMARAS_has_USUARIOS_CAMARAS1`
    FOREIGN KEY (`CAMARAS_idCAMARAS`)
    REFERENCES `proyectonevada`.`camaras` (`idCAMARAS`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_CAMARAS_has_USUARIOS_USUARIOS1`
    FOREIGN KEY (`USUARIOS_idUSUARIOS`)
    REFERENCES `proyectonevada`.`usuarios` (`idUSUARIOS`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `proyectonevada`.`magnitudes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyectonevada`.`magnitudes` (
  `idMAGNITUDES` INT(11) NOT NULL AUTO_INCREMENT,
  `temperatura` FLOAT NULL DEFAULT NULL,
  `fechayhora` MEDIUMTEXT NULL DEFAULT NULL,
  PRIMARY KEY (`idMAGNITUDES`))
ENGINE = InnoDB
AUTO_INCREMENT = 12
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `proyectonevada`.`fotografias`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyectonevada`.`fotografias` (
  `idFOTOGRAFIAS` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NULL DEFAULT NULL,
  `url` VARCHAR(45) NULL DEFAULT NULL,
  `CAMARAS_idCAMARAS` INT(11) NOT NULL,
  `MAGNITUDES_idMAGNITUDES` INT(11) NOT NULL,
  PRIMARY KEY (`idFOTOGRAFIAS`),
  INDEX `fk_FOTOGRAFIAS_CAMARAS_idx` (`CAMARAS_idCAMARAS` ASC),
  INDEX `fk_FOTOGRAFIAS_MAGNITUDES1_idx` (`MAGNITUDES_idMAGNITUDES` ASC),
  CONSTRAINT `fk_FOTOGRAFIAS_CAMARAS`
    FOREIGN KEY (`CAMARAS_idCAMARAS`)
    REFERENCES `proyectonevada`.`camaras` (`idCAMARAS`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_FOTOGRAFIAS_MAGNITUDES1`
    FOREIGN KEY (`MAGNITUDES_idMAGNITUDES`)
    REFERENCES `proyectonevada`.`magnitudes` (`idMAGNITUDES`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
