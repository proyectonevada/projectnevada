package dad.is.trabajo;

import java.util.concurrent.atomic.AtomicInteger;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Magnitud {
	private int idMAGNITUDES;
	private double temperatura;
	private long fechayhora;
	private static final AtomicInteger COUNTER=new AtomicInteger();
	
	
	@JsonCreator
	public Magnitud(
			@JsonProperty("temperatura") double temperatura,  
			@JsonProperty("fechayhora") long fechayhora) {
		super();
		this.idMAGNITUDES = COUNTER.getAndIncrement();
		this.temperatura = temperatura;
		this.fechayhora = fechayhora;
		
	}
	
	
	
	public Magnitud(int idMAGNITUDES, double temperatura, long fechayhora) {
		super();
		this.idMAGNITUDES = idMAGNITUDES;
		this.temperatura = temperatura;
		this.fechayhora = fechayhora;
	}



	public int getId() {
		return idMAGNITUDES;
	}
	public void setId(int idMAGNITUDES) {
		this.idMAGNITUDES = idMAGNITUDES;
	}
	public double getTemperatura() {
		return temperatura;
	}
	public void setTemperatura(Double temperatura) {
		this.temperatura = temperatura;
	}
	public long getFechaHora() {
		return fechayhora;
	}
	public void setFechaHora(long fechayhora) {
		this.fechayhora = fechayhora;
	}
	
	
}
