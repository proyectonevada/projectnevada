package dad.is.trabajo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import java.util.List;



import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.asyncsql.AsyncSQLClient;
import io.vertx.ext.asyncsql.MySQLClient;
import io.vertx.ext.sql.ResultSet;
import io.vertx.ext.sql.SQLConnection;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

public class ApiRest extends AbstractVerticle{
	
	/*private Map<Integer, Fotografias> fotografias=new LinkedHashMap<>();
	private Map<Integer, Camara> camaras=new LinkedHashMap<>();
	private Map<Integer, Magnitud> magnitudes=new LinkedHashMap<>();
	private Map<Integer, Usuario> usuarios=new LinkedHashMap<>();*/
	
	
	SQLConnection connection;
	
	public void start(){
		//createSomeData();
		Router router=Router.router(vertx);
		vertx.createHttpServer().requestHandler(router::accept).listen(8080);
		//CONEXION CON LA BD: 
		JsonObject config = new JsonObject();
		
		config.put("username", "root");
		config.put("password", "raquel");
		
		config.put("host", "localhost");
		config.put("port", 3306);
		config.put("database", "proyectonevada");
		
		AsyncSQLClient mySqlClient = MySQLClient.createShared(vertx, config);
		mySqlClient.getConnection(conn ->{
			if(conn.succeeded()){
				connection = conn.result();
			}else{
				System.out.println(conn.cause().toString());
			}
		});
		
		//FOTOGAFIAS: 
		router.route("/api/fotografias/*").handler(BodyHandler.create());
		router.get("/api/fotografias").handler(this::getAllFotografias);
		router.get("/api/fotografias/:fotoNom").handler(this::getFotografiasByNombre);
		//router.get("/api/fotografias/:idCamara").handler(this::getFotosDeCamara);
		router.put("/api/fotografias").handler(this::addFotografias);
		//CAMARAS:
		router.route("/api/camaras/*").handler(BodyHandler.create());
		router.get("/api/camaras").handler(this::getAllCamaras);
		router.get("/api/camaras/:camId").handler(this::getCamarasById);
		router.put("/api/camaras").handler(this::addCamaras);
		
		//MAGNITUD: 
		router.route("/api/magnitudes/*").handler(BodyHandler.create());
		router.get("/api/magnitudes").handler(this::getAllMagnitudes);
		router.get("/api/magnitudes/:magnId").handler(this::getMagnitudById);
		router.put("/api/magnitudes").handler(this::addMagnitudes);
		
		//USUARIO: 
		router.route("/api/usuarios/*").handler(BodyHandler.create());
		router.get("/api/usuarios").handler(this::getAllUsuarios);
		router.get("/api/usuarios/:UsuLoc").handler(this::getUsuarioByUsername);
		router.get("/api/usuarios/checklogin/:email").handler(this::checkUsuarioPassword);
		router.put("/api/usuarios").handler(this::addUsuarios);
		
		router.delete("/api/fotografias/delete").handler(this::deleteFoto);
		router.delete("/api/camaras/delete").handler(this::deleteCam);
		router.delete("/api/usuarios/delete").handler(this::deleteUsuario);
		router.delete("/api/magnitudes/delete").handler(this::deleteMagnitud);
		
		router.post("/api/usuarios/update/:idUSUARIOS").handler(this::postUsuario);
		router.post("/api/camaras/update/:idCAMARAS").handler(this::postCamara);
		router.post("/api/fotografias/update/:idFOTOGRAFIAS").handler(this::postFoto);
		router.post("/api/magnitudes/update/:idMAGNITUDES").handler(this::postMagnitud);
		
		
		
	}
	
	//TODO: HACER LOS GET QUE NOS VAN A SERVIR YA QUE EL GET QUE ESTA IMPLEMENTADO NO SIRVE.
	//////......FOTOGRAFIAS......///////
	
	/*Este metodo devuelve todas las fotografias almacenadas en la tabla fotografias de la BD:*/
	private void getAllFotografias(RoutingContext routingContext){
		if(connection != null){
			connection.query("SELECT * FROM fotografias;", res -> {
				ResultSet resulSet = res.result();
				List<Fotografias> listResult = new ArrayList<>();
				for(JsonArray temp: resulSet.getResults()){
					Fotografias tempObj = new Fotografias(temp.getInteger(0), temp.getString(1), temp.getString(2), temp.getInteger(3), temp.getInteger(4));
					listResult.add(tempObj);
				}
				routingContext.response().putHeader("content-type","application/json;charset=utf-8").
				end(Json.encodePrettily(listResult));
			});
		}
	}
	
	//Este metodo devuelve una fotografia de la tabla fotografias a partir del nombre:
	private void getFotografiasByNombre(RoutingContext routingContext){
		if(connection != null){
			String fotografia = routingContext.request().getParam("fotoNom");
			JsonArray params = new JsonArray().add(fotografia);
			connection.queryWithParams("SELECT * FROM fotografias WHERE nombre = ? ;",params , res -> {
				ResultSet resulSet = res.result();
				routingContext.response().putHeader("content-type","application/json;charset=utf-8").
				end(Json.encodePrettily(resulSet.getResults()));
			});
		}
	}
	
	//Este metodo a�ade una fotografia con todas sus propiedades a la tabla fotografias de la BD:
	private void addFotografias(RoutingContext routingContext){
		if (connection != null) {
			final Fotografias foto = Json.decodeValue(routingContext.getBodyAsString(), Fotografias.class);
					connection.queryWithParams(
							"INSERT INTO fotografias (nombre,url,idCAMARAS, idMAGNITUDES) VALUES (?,?,?,?);",
							new JsonArray().add(foto.getNombre()).add(foto.getImagen()).add(foto.getCamara()).add(foto.getMagnitud()),
							res -> {
				if (res.succeeded()) {
					routingContext.response().setStatusCode(201)
							.putHeader("content-type", "application/json; charset=utf-8")
							.end(Json.encodePrettily(foto));

				} else {
					routingContext.response().setStatusCode(401).end();
				}
			});
		} else {
			routingContext.response().setStatusCode(401).end();
		}
			
	}
	
	
	
	////////.........CAMARAS..........////////
	/*Este metodo devuelve todas las camaras almacenadas en la tabla camaras de la BD:*/
	private void getAllCamaras(RoutingContext routingContext){
		if(connection != null){
			connection.query("SELECT * FROM camaras;", res -> {
				ResultSet resulSet = res.result();
				List<Camara> listResult = new ArrayList<>();
				for(JsonArray temp: resulSet.getResults()){
					Camara tempObj = new Camara(temp.getInteger(0), temp.getString(1), temp.getFloat(2), temp.getFloat(3),  temp.getFloat(4), temp.getString(5));
					listResult.add(tempObj);
				}
				routingContext.response().putHeader("content-type","application/json;charset=utf-8").
				end(Json.encodePrettily(listResult));
			});
		}
	}
	
	//Este metodo devuelve una camara de la tabla camaras a partir del id:
	private void getCamarasById(RoutingContext routingContext){
		if(connection != null){
			String camara = routingContext.request().getParam("camId");
			JsonArray params = new JsonArray().add(camara);
			connection.queryWithParams("SELECT * FROM camaras WHERE idCAMARAS = ? ;",params , res -> {
				ResultSet resulSet = res.result();
				List<Camara> listResult = new ArrayList<>();
				for(JsonArray temp: resulSet.getResults()){
					Camara camObj = new Camara (temp.getInteger(0), temp.getString(1), temp.getFloat(2), temp.getFloat(3),  temp.getFloat(4), temp.getString(5));
					listResult.add(camObj);
				}
				routingContext.response().putHeader("content-type","application/json;charset=utf-8").
				end(Json.encodePrettily(listResult));
			});
		}
	}
	
	//Este metodo a�ade una camara con todas sus propiedades a la tabla camara de la BD:
	private void addCamaras(RoutingContext routingContext){
		if (connection != null) {
			final Camara cam = Json.decodeValue(routingContext.getBodyAsString(), Camara.class);
					connection.queryWithParams(
							"INSERT INTO camaras (nombre,altitud,longitud,latitud,descripcion) VALUES (?,?,?,?,?);",
							new JsonArray().add(cam.getNombre()).add(cam.getAltitud()).add(cam.getLongitud()).add(cam.getLatitud()).add(cam.getDescripcion()),
							res -> {
				if (res.succeeded()) {
					routingContext.response().setStatusCode(201)
							.putHeader("content-type", "application/json; charset=utf-8")
							.end(Json.encodePrettily(cam));

				} else {
					routingContext.response().setStatusCode(401).end();
				}
			});
		} else {
			routingContext.response().setStatusCode(401).end();
		}
	}
	
		
	///////.........MAGNITUDES........///////
	/*Este metodo devuelve todas las magnitudes almacenadas en la tabla magnitudes de la BD:*/
	private void getAllMagnitudes(RoutingContext routingContext){
		if(connection != null){
			connection.query("SELECT * FROM magnitudes;", res -> {
				ResultSet resulSet = res.result();
				List<Magnitud> listResult = new ArrayList<>();
				for(JsonArray temp: resulSet.getResults()){
					Magnitud tempObj = new Magnitud(temp.getInteger(0), temp.getDouble(1), temp.getLong(2));
					listResult.add(tempObj);
				}
				routingContext.response().putHeader("content-type","application/json;charset=utf-8").
				end(Json.encodePrettily(listResult));
			});
		}
	}
	
	//Este metodo devuelve una magnitud de la tabla magnitudes a partir del id:
	private void getMagnitudById(RoutingContext routingContext){
		if(connection != null){
			String magnitud = routingContext.request().getParam("magnId");
			JsonArray params = new JsonArray().add(magnitud);
			connection.queryWithParams("SELECT * FROM magnitudes WHERE idMAGNITUDES = ? ;",params , res -> {
				ResultSet resultSet = res.result();
				
				routingContext.response().putHeader("content-type","application/json;charset=utf-8").
				end(Json.encodePrettily(resultSet.getResults()));
			});
		}
	}
	
	
	//Este metodo a�ade una magnitud con todas sus propiedades a la tabla magnitudess de la BD:
	private void addMagnitudes(RoutingContext routingContext){
		if (connection != null) {
			final Magnitud mag = Json.decodeValue(routingContext.getBodyAsString(), Magnitud.class);
			String pattern = "yyyy-MM-dd HH:mm:ss";
			SimpleDateFormat formatter = new SimpleDateFormat(pattern);
			String dateString = formatter.format(mag.getFechaHora());
					connection.queryWithParams(
							"INSERT INTO magnitudes (temperatura,fechayhora) VALUES (?,?);",
							new JsonArray().add(mag.getTemperatura()).add(dateString),
							res -> {
				if (res.succeeded()) {
					routingContext.response().setStatusCode(201)
							.putHeader("content-type", "application/json; charset=utf-8")
							.end(Json.encodePrettily(mag));

				} else {
					routingContext.response().setStatusCode(401).end();
				}
			});
		} else {
			routingContext.response().setStatusCode(401).end();
		}
	}
	
	
	///////.......USUARIOS....../////
	/*Este metodo devuelve todas los usuarios almacenadas en la tabla usuarios de la BD:*/
	private void getAllUsuarios(RoutingContext routingContext){
		if(connection != null){
			connection.query("SELECT * FROM usuarios;", res -> {
				ResultSet resulSet = res.result();
				routingContext.response().putHeader("content-type","application/json;charset=utf-8").
				end(Json.encodePrettily(resulSet.getResults()));
			});
		}
	}
	
	//Este metodo devuelve unusuario de la tablausuarios a partir del username:
	private void getUsuarioByUsername(RoutingContext routingContext){
		if(connection != null){
			String usuario = routingContext.request().getParam("UsuLoc");
			JsonArray params = new JsonArray().add(usuario);
			connection.queryWithParams("SELECT * FROM usuarios WHERE usuario = ? ;",params , res -> {
				ResultSet resulSet = res.result();
				routingContext.response().putHeader("content-type","application/json;charset=utf-8").
				end(Json.encodePrettily(resulSet.getResults()));
			});
		}
	}
	
	//Este metodo a�ade un usuario con todas sus propiedades a la tabla usuarios de la BD:
	private void addUsuarios(RoutingContext routingContext){
		if (connection != null) {
			final Usuario usuario = Json.decodeValue(routingContext.getBodyAsString(), Usuario.class);
					connection.queryWithParams(
							"INSERT INTO usuarios (usuario,contrase�a,nombre,apellidos,email) VALUES (?,?,?,?,?);",
							new JsonArray().add(usuario.getUsuario()).add(usuario.getPassword())
									.add(usuario.getNombre()).add(usuario.getApellidos()).add(usuario.getEmail()),
							res -> {
				if (res.succeeded()) {
					routingContext.response().setStatusCode(201)
							.putHeader("content-type", "application/json; charset=utf-8")
							.end(Json.encodePrettily(usuario));

				} else {
					routingContext.response().setStatusCode(401).end();
				}
			});
		} else {
			routingContext.response().setStatusCode(401).end();
		}
	}
	
	//Comprueba que existe un usuario: Si el usuario esta en la BD devuelve el usuario que contenga tanto el email como la contrase�a 
	//que pasamos por parametro desde la url si no existe, devuelve un error 401.
	private void checkUsuarioPassword(RoutingContext routingContext) {
			if (connection != null) {
				String email = routingContext.request().getParam("email");
				String md5 = routingContext.request().getParam("passMD5");
				connection.queryWithParams(
						"SELECT * FROM usuarios WHERE email = ? AND password = ?;",
						new JsonArray().add(email).add(md5), res -> {
					ResultSet resultSet = res.result();
					if (res.succeeded() && resultSet.getNumRows() > 0) {
						routingContext.response().putHeader("content-type", "application/json; charset=utf-8")
								.end(Json.encodePrettily(resultSet.getRows().get(0)));
					} else {
						routingContext.response().setStatusCode(401).end();
					}
				});
			} else {
				routingContext.response().setStatusCode(401).end();
			}
		}
	
	
	//Los POST
	
		//Actualiza una tupla (seg�n su id) de la tabla MAGNITUDES.
		private void postMagnitud(RoutingContext routingContext) {
			if (connection != null) {
					int id = Integer.parseInt(routingContext.request().getParam("idMAGNITUDES"));
					final Magnitud magnitud = Json.decodeValue(routingContext.getBodyAsString(), Magnitud.class);
					connection.queryWithParams(
							"UPDATE fotografias SET idMAGNITUDES = ?,"
							+ "temperatura=?,fechayhora=?, "
							+ "WHERE idMAGNITUDES=?",
							new JsonArray().add(magnitud.getId())
							.add(magnitud.getTemperatura()).add(magnitud.getFechaHora())
							.add(id),
							res -> {
						if (res.succeeded()) {
							routingContext.response().setStatusCode(201)
									.putHeader("content-type", "application/json; charset=utf-8")
									.end(Json.encodePrettily(magnitud));

						} else {
							routingContext.response().setStatusCode(401).end();
						}
					});
				} else {
					routingContext.response().setStatusCode(401).end();
				}
			}

		
		
		//Actualiza una tupla (seg�n su id) de la tabla FOTOGRAFIAS.
		private void postFoto(RoutingContext routingContext) {
				if (connection != null) {
					int id = Integer.parseInt(routingContext.request().getParam("idFOTOGRAFIAS"));
					final Fotografias foto = Json.decodeValue(routingContext.getBodyAsString(), Fotografias.class);
					connection.queryWithParams(
							"UPDATE fotografias SET idFOTOGRAFIAS = ?,"
							+ "nombre=?,url=?,CAMARAS_idCAMARAS=?,MAGNITUDES_idMAGNITUDES=? "
							+ "WHERE idFOTOGRAFIAS=?",
							new JsonArray().add(foto.getId()).add(foto.getId())
							.add(foto.getNombre()).add(foto.getImagen())
							.add(foto.getCamara()).add(foto.getMagnitud()).add(id),
							res -> {
						if (res.succeeded()) {
							routingContext.response().setStatusCode(201)
									.putHeader("content-type", "application/json; charset=utf-8")
									.end(Json.encodePrettily(foto));

						} else {
							routingContext.response().setStatusCode(401).end();
						}
					});
				} else {
					routingContext.response().setStatusCode(401).end();
				}
			}
		
		//Actualiza una tupla (seg�n su id) de la tabla CAMARAS.
		private void postCamara(RoutingContext routingContext) {
			if (connection != null) {
					int id = Integer.parseInt(routingContext.request().getParam("idCAMARAS"));
					final Camara camara = Json.decodeValue(routingContext.getBodyAsString(), Camara.class);
					connection.queryWithParams(
							"UPDATE camaras SET idCAMARAS = ?, altitud = ?,"
							+ "longitud = ?, latitud = ?, descripcion = ?"
							+ "WHERE idCAMARAS=?",
							new JsonArray().add(camara.getId()).add(camara.getAltitud())
							.add(camara.getLongitud()).add(camara.getLatitud())
							.add(camara.getDescripcion()).add(id),
							res -> {
						if (res.succeeded()) {
							routingContext.response().setStatusCode(201)
									.putHeader("content-type", "application/json; charset=utf-8")
									.end(Json.encodePrettily(camara));

						} else {
							routingContext.response().setStatusCode(401).end();
						}
					});
				} else {
					routingContext.response().setStatusCode(401).end();
				}
			}

		
		
		//Actualiza una tupla (seg�n su id) de la tabla USUARIOS.
		private void postUsuario(RoutingContext routingContext) {
			if (connection != null) {
					int id = Integer.parseInt(routingContext.request().getParam("idUSUARIOS"));
					final Usuario usuario = Json.decodeValue(routingContext.getBodyAsString(), Usuario.class);
					connection.queryWithParams(
							"UPDATE usuarios SET idUSUARIOS = ?, usuario = ?,"
							+ "contrase�a = ?, nombre = ?, apellidos = ?, correo = ? "
							+ "WHERE idUSUARIOS=?",
							new JsonArray().add(usuario.getId()).add(usuario.getUsuario())
							.add(usuario.getPassword()).add(usuario.getNombre())
							.add(usuario.getApellidos()).add(usuario.getEmail()).add(id),
							res -> {
						if (res.succeeded()) {
							routingContext.response().setStatusCode(201)
									.putHeader("content-type", "application/json; charset=utf-8")
									.end(Json.encodePrettily(usuario));

						} else {
							routingContext.response().setStatusCode(401).end();
						}
					});
				} else {
					routingContext.response().setStatusCode(401).end();
				}
			}
		
		
		//Los DELETE
		
		//Elimina una tupla (seg�n su id) de la tabla MAGNITUDES.
		private void deleteMagnitud(RoutingContext r){
			if (connection != null) {
					final Magnitud magnitud=Json.decodeValue(r.getBodyAsString(), Magnitud.class);
					connection.queryWithParams("DELETE FROM magnitudes WHERE idMAGNITUDES=?",
							new JsonArray().add(magnitud.getId()), res->{
								if(res.succeeded()){
									r.response().setStatusCode(201)
										.putHeader("content-type", "application/json; charset=utf-8")
										.end(Json.encodePrettily(magnitud));
								}else{
									r.response().setStatusCode(401).end();
								}
							});
				}else{
					r.response().setStatusCode(401).end();
				}
		}
		//Elimina una tupla (seg�n su id) de la tabla USUARIOS.
		private void deleteUsuario(RoutingContext r){
			if (connection != null) {
					final Usuario usuario=Json.decodeValue(r.getBodyAsString(), Usuario.class);
					connection.queryWithParams("DELETE FROM usuarios WHERE idUSUARIOS=?",
							new JsonArray().add(usuario.getId()), res->{
								if(res.succeeded()){
									r.response().setStatusCode(201)
										.putHeader("content-type", "application/json; charset=utf-8")
										.end(Json.encodePrettily(usuario));
								}else{
									r.response().setStatusCode(401).end();
								}
							});
				}else{
					r.response().setStatusCode(401).end();
				}
			}
		//Elimina una tupla (seg�n su id) de la tabla CAMARAS.
		private void deleteCam(RoutingContext r){
			if (connection != null) {
					final Camara camara=Json.decodeValue(r.getBodyAsString(), Camara.class);
					connection.queryWithParams("DELETE FROM camaras WHERE idCAMARAS=?",
							new JsonArray().add(camara.getId()), res->{
								if(res.succeeded()){
									r.response().setStatusCode(201)
										.putHeader("content-type", "application/json; charset=utf-8")
										.end(Json.encodePrettily(camara));
								}else{
									r.response().setStatusCode(401).end();
								}
							});
				}else{
					r.response().setStatusCode(401).end();
				}
			}
		
		//Elimina una tupla (seg�n su id) de la tabla FOTOGRAFIAS.
		private void deleteFoto(RoutingContext r){
			if (connection != null) {
					final Fotografias foto=Json.decodeValue(r.getBodyAsString(), Fotografias.class);
					connection.queryWithParams("DELETE FROM FOTOGRAFIAS WHERE idFOTOGRAFIAS=?",
							new JsonArray().add(foto.getId()), res->{
								if(res.succeeded()){
									r.response().setStatusCode(201)
										.putHeader("content-type", "application/json; charset=utf-8")
										.end(Json.encodePrettily(foto));
								}else{
									r.response().setStatusCode(401).end();
								}
							});
				}else{
					r.response().setStatusCode(401).end();
				}
			}
}
