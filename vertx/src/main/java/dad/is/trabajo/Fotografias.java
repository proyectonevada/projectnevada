package dad.is.trabajo;

import java.util.concurrent.atomic.AtomicInteger;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Fotografias {
	private int idFOTOGRAFIAS;
	private String url;
	private String nombre;
	private int CAMARAS_idCAMARAS;
	private int MAGNITUDES_idMAGNITUDES;
	private static final AtomicInteger COUNTER=new AtomicInteger();
	
	
	@JsonCreator
	public Fotografias(
			@JsonProperty("nombre") String nombre,
			@JsonProperty("url") String url,
			@JsonProperty("CAMARAS_idCAMARAS") int CAMARAS_idCAMARAS,
			@JsonProperty("MAGNITUDES_idMAGNITUDES") int MAGNITUDES_idMAGNITUDES) {

		this.idFOTOGRAFIAS = COUNTER.getAndIncrement();
		this.url = url;
		this.nombre = nombre;
		this.CAMARAS_idCAMARAS = CAMARAS_idCAMARAS;
		this.MAGNITUDES_idMAGNITUDES = MAGNITUDES_idMAGNITUDES;
	}
	
	
	
	public Fotografias(int idFOTOGRAFIAS, String url, String nombre, int cAMARAS_idCAMARAS,
			int mAGNITUDES_idMAGNITUDES) {
		super();
		this.idFOTOGRAFIAS = idFOTOGRAFIAS;
		this.url = url;
		this.nombre = nombre;
		CAMARAS_idCAMARAS = cAMARAS_idCAMARAS;
		MAGNITUDES_idMAGNITUDES = mAGNITUDES_idMAGNITUDES;
	}

	
	public int getId() {
		return idFOTOGRAFIAS;
	}
	public void setId(int idFOTOGRAFIAS) {
		this.idFOTOGRAFIAS = idFOTOGRAFIAS;
	}
	public String getImagen() {
		return url;
	}
	public void setImagen(String url) {
		this.url = url;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getCamara() {
		return CAMARAS_idCAMARAS;
	}
	public void setCamara(int CAMARAS_idCAMARAS) {
		this.CAMARAS_idCAMARAS = CAMARAS_idCAMARAS;
	}
	
	public int getMagnitud(){
		return MAGNITUDES_idMAGNITUDES;
	}
	
	public void setMagnitud(int MAGNITUDES_idMAGNITUDES){
		this.MAGNITUDES_idMAGNITUDES=MAGNITUDES_idMAGNITUDES;
	}
	
	
	
}
