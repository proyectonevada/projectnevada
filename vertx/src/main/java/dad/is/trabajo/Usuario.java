package dad.is.trabajo;

import java.util.concurrent.atomic.AtomicInteger;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Usuario {
	private int idUSUARIOS;
	private String usuario;
	private String contraseņa;
	private String nombre;
	private String apellidos;
	private String correo;
	private static final AtomicInteger COUNTER=new AtomicInteger();
	
	@JsonCreator
	public Usuario(
			@JsonProperty("usuario") String usuario,
			@JsonProperty("contraseņa") String contraseņa,
			@JsonProperty("nombre") String nombre,
			@JsonProperty("apellidos") String apellidos,
			@JsonProperty("correo") String correo){
			 		
		super();
		this.idUSUARIOS=COUNTER.getAndIncrement();
		this.usuario = usuario;
		this.contraseņa = contraseņa;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.correo =correo;
	}
	
	
	
	public Usuario(int idUSUARIOS, String usuario, String contraseņa, String nombre, String apellidos, String correo) {
		super();
		this.idUSUARIOS = idUSUARIOS;
		this.usuario = usuario;
		this.contraseņa = contraseņa;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.correo = correo;
	}



	public int getId() {
		return idUSUARIOS;
	}
	public void setId(int idUSUARIOS) {
		this.idUSUARIOS = idUSUARIOS;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getPassword() {
		return contraseņa;
	}
	public void setPassword(String contraseņa) {
		this.contraseņa = contraseņa;
	}
	public String getNombre(){
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidos(){
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public String getEmail(){
		return correo;
	}
	public void setEmail(String correo) {
		this.correo = correo;
	}
	
	
}
