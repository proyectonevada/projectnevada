package dad.is.trabajo;

import java.util.concurrent.atomic.AtomicInteger;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Camara {
	private int idCAMARAS;
	private String nombre;
	private float altitud;
	private float latitud;
	private float longitud;
	private String descripcion;
	private static final AtomicInteger COUNTER=new AtomicInteger();
	
	
	@JsonCreator
	public Camara(
			@JsonProperty("nombre") String nombre,
			@JsonProperty("altitud") float altitud,
			@JsonProperty("longitud") float longitud,
			@JsonProperty("latitud") float latitud,
			@JsonProperty("descripcion") String descripcion){
		super();
		this.idCAMARAS = COUNTER.getAndIncrement();
		this.nombre= nombre;
		this.altitud = altitud;
		this.latitud = latitud;
		this.longitud = longitud;
		this.descripcion = descripcion;
	}
	
	
	public Camara(int idCAMARAS, String nombre,  float altitud, float latitud, float longitud, String descripcion) {
		super();
		this.idCAMARAS = idCAMARAS;
		this.nombre= nombre;
		this.altitud = altitud;
		this.latitud = latitud;
		this.longitud = longitud;
		this.descripcion = descripcion;
	}




	public int getId(){
		return idCAMARAS;
	}
	
	public void setId(int idCAMARAS){
		this.idCAMARAS =idCAMARAS;
	}
	

	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public double getAltitud() {
		return altitud;
	}
	public void setAltitud(float altitud) {
		this.altitud = altitud;
	}
	public float getLatitud() {
		return latitud;
	}
	public void setLatitud(float latitud) {
		this.latitud = latitud;
	}
	public float getLongitud() {
		return longitud;
	}
	public void setLongitud(float longitud) {
		this.longitud = longitud;
	}
	public String getDescripcion(){
		return descripcion;
		
	}
	public void setDescripcion(String descripcion){
		this.descripcion = descripcion;
	}
	
	
	
}
