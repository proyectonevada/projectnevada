package dad.is.clase.tcp;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.net.NetClient;
import io.vertx.core.net.NetClientOptions;
import io.vertx.core.net.NetSocket;

public class DadTcpClient extends AbstractVerticle {

	public void start(){
		NetClientOptions options=new NetClientOptions();
		options.setConnectTimeout(10000);
		//si falla, 5 intentos para reconectar
		options.setReconnectAttempts(5);
		//un segundo para cada intento
		options.setReconnectInterval(1000);
		
		NetClient client=vertx.createNetClient(options);
		client.connect(8083, "localHost",res->{
			NetSocket socket= res.result();
			socket.handler(msg->{
				System.out.println(msg.toString());
				//con socket.write le envia una respuesta al servidor
				socket.write("Bien! Me ha llegado el mensaje"+this.getClass().getName());
			});
		});
	}
}
