package dad.is.clase.tcp;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.net.NetServer;
import io.vertx.core.net.NetServerOptions;

public class DadTcpServer extends AbstractVerticle {
	public void start(Future<Void> startFuture){
		NetServerOptions options=new NetServerOptions();
		options.setPort(8083);
		NetServer server=vertx.createNetServer(options);
		server.connectHandler(connection->{
			System.out.println(connection.localAddress().toString());
			connection.write("Hola cliente");
			connection.handler(msg->{
				System.out.println(msg.toString());
			});
			connection.write("Hola cliente"+this.getClass().getName() + "\n");
		});
		server.listen(status->{
			if(status.succeeded()){
				System.out.println("Servidor desplegado");
			}else{
				System.out.println("Servidor no desplegado");
			}
		});
		vertx.deployVerticle(DadTcpClient.class.getName());
	}

}
