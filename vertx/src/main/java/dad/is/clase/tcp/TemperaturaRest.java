package dad.is.clase.tcp;

import java.util.concurrent.atomic.AtomicInteger;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TemperaturaRest {

	private int valor;
	private String localizacion;
	private int id;
	private static final AtomicInteger COUNTER=new AtomicInteger();

	
	
	@JsonCreator
	public TemperaturaRest(@JsonProperty ("value") int valor, 
			@JsonProperty("localizacion")String localizacion) {
		super();
		this.valor = valor;
		this.localizacion = localizacion;
		this.id = COUNTER.getAndIncrement();
	}

		public TemperaturaRest(int id,int valor,String localizacion){
			super();
			this.id=id;
			this.valor=valor;
			this.localizacion=localizacion;
		}


	public int getValor() {
		return valor;
	}



	public void setValor(int valor) {
		this.valor = valor;
	}



	public String getLocalizacion() {
		return localizacion;
	}



	public void setLocalizacion(String localizacion) {
		this.localizacion = localizacion;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	
}	
