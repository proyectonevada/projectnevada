package dad.is.clase.tcp;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.asyncsql.AsyncSQLClient;
import io.vertx.ext.asyncsql.MySQLClient;
import io.vertx.ext.sql.ResultSet;
import io.vertx.ext.sql.SQLConnection;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

     


public class ApiRestServer extends AbstractVerticle {

	private Map<Integer,TemperaturaRest> temperaturas=new LinkedHashMap<>();
	
	SQLConnection connection;
	
	public void start(){
		createSomeData();
		Router router=Router.router(vertx);
		vertx.createHttpServer().requestHandler(router::accept).listen(8080);
		
		JsonObject config=new JsonObject();
		config.put("username","root");
		config.put("password", "donmanue");
		config.put("host","localhost");
		config.put("port", 3306);
		config.put("database", "dad");
		
		AsyncSQLClient mySQLClient=MySQLClient.createShared(vertx, config);
		mySQLClient.getConnection(conn->{
			if(conn.succeeded()){
				connection=conn.result();
			}else{
				System.out.println(conn.cause().toString());
			}
		});
		
		
		router.route("/api/temperature/*").handler(BodyHandler.create());
		router.get("/api/temperature").handler(this::getAllTemperature);
		router.get("/api/temperature/:tempLoc").handler(this::getTemperatureByLoc);

		
		router.put("/api/temperature").handler(this::addTemperature);
		router.delete("/api/temperature").handler(this::deleteTemperature);
		router.post("/api/temperature/:temperatureId").handler(this::updateTemperature);


		//router.get("/api/humedad").handler(this::getAllTemperature);
		//router.get("/api/presion").handler(this::getAllTemperature);

	}
	//ponemos void porque nos lo va a devolver todo en la peticion
	private void getAllTemperature(RoutingContext routingContext){
		if(connection!=null){
			connection.query("SELECT * FROM temperature;", res->{
				ResultSet resultSet=res.result();
				List<TemperaturaRest> listResult=new ArrayList<>();
				
				for(JsonArray temp:resultSet.getResults()){
					TemperaturaRest tempObj=new TemperaturaRest(temp.getInteger(0)
							,temp.getInteger(1),temp.getString(2));
					listResult.add(tempObj);
				}
				
			});
		}
		
		routingContext.response().putHeader("content-type","application/json;charset=utf-8").
		end(Json.encodePrettily(temperaturas.values()));
	}
	
	
	
	private void getTemperatureByLoc(RoutingContext routingContext){
		if(connection!=null){
			String location=routingContext.request().getParam("temploc");
			JsonArray params =new JsonArray().add(location);
			
			connection.queryWithParams("SELECT * FROM temperature WHERE localizacion=? AND valor=?;",params, res->{
				ResultSet resultSet=res.result();
				List<TemperaturaRest> listResult=new ArrayList<>();
				
				for(JsonArray temp:resultSet.getResults()){
					TemperaturaRest tempObj=new TemperaturaRest(temp.getInteger(0)
							,temp.getInteger(1),temp.getString(2));
					listResult.add(tempObj);
				}
				
			});
		}
	}
	
	
	
	
	
	
	
	
	private void addTemperature(RoutingContext routingContext){
	TemperaturaRest t=Json.decodeValue(routingContext.getBodyAsString(),
			TemperaturaRest.class);
	temperaturas.put(t.getId(),t);
	routingContext.response().setStatusCode(201).putHeader("content-type",
			"application/json;charset=utf-8").
	end(Json.encodePrettily(t));
	}
	
	private void deleteTemperature(RoutingContext routingContext){
		TemperaturaRest t=Json.decodeValue(routingContext.getBodyAsString(),
				TemperaturaRest.class);
	TemperaturaRest tDeleted=temperaturas.remove(t.getId());
	routingContext.response().setStatusCode(201).putHeader("content-type",
			"application/json;charset=utf-8").end(Json.encodePrettily(tDeleted));
	}
	
	private void updateTemperature(RoutingContext routingContext){
		int id=Integer.parseInt(routingContext.request().getParam("temperatureId"));
		TemperaturaRest toriginal=temperaturas.get(id);
		if(toriginal!=null){
			TemperaturaRest t=Json.decodeValue(routingContext.getBodyAsString(),
					TemperaturaRest.class);
			toriginal.setLocalizacion(t.getLocalizacion());
			toriginal.setValor(t.getValor());
			temperaturas.put(toriginal.getId(), toriginal);
			routingContext.response().setStatusCode(201).putHeader("content-type",
					"application/json;charset=utf-8").end(Json.encodePrettily(toriginal));
			
		}else{
			routingContext.response().setStatusCode(401).end();
		}
	}
	
	private void createSomeData(){
		TemperaturaRest t1=new TemperaturaRest(35,"Sevilla");
		TemperaturaRest t2=new TemperaturaRest(30,"Cadiz");
		TemperaturaRest t3=new TemperaturaRest(35,"Bilbao");
		temperaturas.put(t1.getId(), t1);
		temperaturas.put(t2.getId(), t2);
		temperaturas.put(t3.getId(), t3);

	}
}
