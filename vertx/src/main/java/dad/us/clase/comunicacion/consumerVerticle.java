package dad.us.clase.comunicacion;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;

public class consumerVerticle extends AbstractVerticle{
	public void start (Future<Void> startfuture){
		vertx.eventBus().consumer("mensaje-broadcast",message->{
			System.out.println(message.headers().toString());
			System.out.println(message.body());
		});
		vertx.eventBus().consumer("mensaje-p2p",message->{
			System.out.println(message.body());
			message.reply("s�, te he escuchado");
		});
	}
}
