package dad.us.clase.comunicacion;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.MultiMap;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.EventBus;

public class senderVerticle extends AbstractVerticle {

	@Override
	public void start(Future<Void> startFuture) throws Exception {
		
		EventBus eventBus=vertx.eventBus();
		vertx.setPeriodic(2000, _t->{

			MultiMap headers=MultiMap.caseInsensitiveMultiMap();
			headers.add("Content-Type","application/json");
			headers.add("Descripcion", "ejemplo de cabecera");
			
			
			DeliveryOptions options=new DeliveryOptions();
			options.setHeaders(headers);
			eventBus.publish("mensaje-broadcast","Soy un broadcast,�alguien me escucha?");
		});
		
		vertx.setPeriodic(4000, _t->{
			eventBus.send("mensaje-p2p","Soy punto a punto,�me escuchas?",reply->{
				System.out.println(reply.result().body());
			});
		});
		
		
		
		
		vertx.deployVerticle(consumerVerticle.class.getName(),deployResult ->{
			if(deployResult.succeeded()){//esto es que todo ha ido bien))
			System.out.println("consumerVerticle lanzado con exito");
			}else{
				System.out.println("Error al desplegar consumerVerticle");
			}
		});
		
	}

	@Override
	public void stop(Future<Void> stopFuture) throws Exception {
		// TODO Auto-generated method stub
	}
	

}
