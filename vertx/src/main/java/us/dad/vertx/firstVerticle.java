package us.dad.vertx;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;

public class firstVerticle extends AbstractVerticle{

	@Override
	public void start(Future<Void> startFuture) throws Exception {

		vertx.createHttpServer().requestHandler(r->{r.response().end("<h1>Bienvenido</h1>"
				+ "Este es mi primer verticle");}).listen(8089,res->{
					if(res.failed()){
					startFuture.complete();}else{
						startFuture.fail(res.cause());
					}
				});
	}

	
}
